//lab9

#include <SDL/SDL.h>
#include <math.h>

//gcc sdl.c -lmingw32 -lsdlmain -lsdl
//https://www.cs.utah.edu/~germain/PPS/Topics/color.html

/*color options
  0xffd700 gold
  0x4b0082 indigo
  0x7a7a7a gray
  0xffffff white
  0x000000 black
  0xff0000 red
  0x00ffff aqua
  0x0000ff blue
*/

int main( int argc, char *argv[] ) {
   
   ////////////////////////
   // SDL Initialization //
   ////////////////////////
   SDL_Surface *screen;
   atexit( SDL_Quit );
   if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) exit( 1 );
   SDL_WM_SetCaption( "Data Analysis", NULL ); // Data analysis //title
   screen = SDL_SetVideoMode( 700 , 700 , 32 , SDL_DOUBLEBUF|SDL_HWSURFACE|SDL_ANYFORMAT ); //640,480,32 //screen size
   SDL_FillRect( screen , NULL , 0x0000ff ); //0x205000 //Background color
   SDL_LockSurface( screen );
   int bpp = screen->format->BytesPerPixel;
   Uint32 red = SDL_MapRGB( screen->format, 0xff, 0, 0xff );

   ///////////////////
   // Data Plotting //
   ///////////////////
   //int data[ 8 ][ 2 ] = { 10, 20, 50 ,40, 100, 30, 150, 50, 200, 55 };
   int data[ 9 ][ 2 ] = { 200, 80, 500, 80, 600, 250, 500, 500, 200, 500, 400, 250};
   Uint8 *p;
   double x, x0, x1, y, y0, y1, length, deltax, deltay;   
   for( int count = 0; count < 4; count++ ) {
      x0 = data[ count ][ 0 ];
      y0 = data[ count ][ 1 ];
      x1 = data[ count + 1 ][ 0 ];
      y1 = data[ count + 1 ][ 1 ];
      x = x1 - x0;
      y = y1 - y0;
      length = sqrt( x*x + y*y );
      deltax = x / length;
      deltay = y / length;
      x = x0;
      y = y0;
      for( int i = 0; i < length; i++ ) {
         p = (Uint8 *)screen->pixels + (int)y * screen->pitch + (int)x * bpp;
         *(Uint32 *)p = red;
         x += deltax;
         y += deltay;
      }
   }
   SDL_UnlockSurface(screen);

   SDL_Flip(screen);

   ////////////////////
   // Event Handling //
   ////////////////////
   SDL_Event event;
   while( 1 ) {
      SDL_PollEvent( &event );
      if( event.type == SDL_KEYUP )
         if( event.key.keysym.sym == SDLK_UP ) break;
   }
   
   while( 1 )
   return 0;
}