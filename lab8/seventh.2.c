//lab8

#include <stdio.h>
#include <stdlib.h>

struct item {
   char * value;
   struct item * next;
   struct item * previous;
};

int main( int argc, char *argv[ ] ) { 
   char * string;
   string = "rice";
   struct item a;
   a.value = string;
   a.next = NULL;
   a.previous = NULL;

   string = "spinach";
   struct item b;
   b.value = string;
   b.next = NULL;
   b.previous = &a;
   a.next = &b;

   string = "salmon";
   struct item c;
   c.value = string;
   c.next = NULL;
   c.previous = &b;
   b.next = &c;
   
   struct item * list;
   list = &a;
   printf( "%s\n", (*list).value );
   printf( "%s\n", list->value );

   printf( "%s\n", list->next->value );
   printf( "%s\n", list->next->next->value );

   list = list->next;
   printf( "%s\n", list->value );
   list = list->next;
   printf( "%s\n", list->value );
   list = list->previous;
   printf( "%s\n", list->value );
   
   ////////////////////////
   // Adding a new item  //
   ////////////////////////
   string = "cauliflower";
   struct item d;
   d.value = string;
   d.next = NULL;
   d.previous = &c;
   c.next = &d;
   
   //////////////////////////
   // Rearranging the list //
   //////////////////////////
   a.next = &c;
   a.previous = &b;
   b.next = &a;
   b.previous = &d;
   c.next = NULL;
   c.previous = &a;
   d.next = &b;
   d.previous = NULL;
   ////////////////////////////////
   // Display the list backwards //
   ////////////////////////////////
   printf("%s ", c.value);
   printf("%s ", a.value);
   printf("%s ", b.value);
   printf("%s", d.value);
   printf("\n");
   ///////////////////////////
   // Display list forwards //
   ///////////////////////////
   list = &d;
   int count = 0;
   for(count=0; count<4; count++)
	{
	printf("%s ",(* list).value);
	list = list->next;
	}

   return 0;
}