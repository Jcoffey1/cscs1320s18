#include <stdio.h>
int main( int arc, char * argv[ ] ) {
//       	( int arc, char * argv[ ] will be in every code for C the first { starts your code
int age=27;
//declaration^ of an int named age and an assigning it the value of 27
char* name="Joe Coffey";
//declaration of a char pointer named name assigning the the value of "Joe Coffey"
//char* are associated with strings
float pie=3.1415;
//float is a declaration for a value
double dpie=3.1415;
//double is a bigger more precise float
char letter='j';
//char is used for single values in singles qoutes ''
printf ( "%s is %d and likes %f %6.4f %lf %c \n",name ,age, pie, pie, dpie, letter) ;
//%s is saying this is where the string is going and is coming from name
//%d is stating where the number value is going that has been assigned to age
//values have to be assigned in order... %s with name, %d with age.
//%f is assigned with the floating value
//%lf is assigned to the double floating value
//%(some numerical parameter)lf is setting a parameter to how many numbers the command program will printf
//%6.4lf is stating to print the lf declaration with 6 characters and 4 after the decimal
//\n is ending the line in the command prompt and starting a new one
double first= 2;
//double declaration with the name first and the value assigned of 2
double second= 3;
double third= 4;
double amount= 3;
double total= first+second+third ;
//double declaration with the name total and the value of the assigned names added
printf ( "%1.0lf + %1.0lf + %1.0lf = %1.0lf \n", first ,second ,third ,total) ;
//%1.0lf is setting the parameter of how many numbers you want printed from the declaration
double total2= (first+second+third)/amount;
//double declaration with the name total2 and the value of the assigned names added and than divided by
// the assigned amount value (how many declarations there are) to find the average
//the math has to be done by assignments through paranthesis. (addition)/amount is telling the command prompt to
//do whats in the paranthesis first and than what is outside the paranthesis
printf ( "%1.0lf + %1.0lf + %1.0lf / %1.0lf = %1.3lf \n", first ,second ,third ,amount ,total2) ;
//%1.3lf is setting a wide parameter for what is printed in case there is a decimal value for the total
return 0 ;
}