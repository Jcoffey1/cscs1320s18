
include <stdio.h>
include <iostream>
include <iomanip>

using std::cout;


using namespace std;

int main( int argc, char * argv[ ] ) {


printf( "hello, from printf\n" );


//	std::cout << "hello, from cout" << std::endl;
cout << "hello, from cout; pi: " << setw( 15 ) << left << setprecision( 3 ) << 3.1415 << endl;
cout << "pi: " << setw( 15 ) << left << setprecision( 3 ) << scientific << 3.1415 << endl;
cout << "pi: " << setw( 15 ) << left << setprecision( 3 ) << fixed << hex << 31 << endl;
return 0;
}