#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main( ) { 
  string str1 ( "string type" );
  string str2 ( "char type" );

  if( str1.compare( str2 ) != 0 )
    cout << str1 << " is not a " << str2;

  if ( str1.compare( 7, 4, str2, 5, 4 ) == 0 )
    cout << ", but both are types." << endl;

  cout << str1 << "'s type is " << str1.substr( 0, 6 ) << endl;

  cout << "ing appears in " << str1 << " at position " << str1.find( "ing" );

  return 0;
}