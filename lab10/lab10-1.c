#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

int main( ) { 
    int integer;
    double real = 8.059;
    string word = "javaj";
bool joe = true;
	cout << "joe" << boolalpha << setw(8) << joe << endl;
	cout << "joe" << noboolalpha << setw(8) << joe << endl;
	cout << real << endl;
	cout << fixed << setprecision(2) << real << endl;
	cout << word << endl;
	cout << setw(10) << setfill('*') << word << endl;
    cout << "enter an integer: " << oct;
    cin >> integer >> dec;
    cout << "integer: " << integer << endl;

    cout << "enter a real number: ";
    cin >> real >> scientific >> showpos;
    cout << "real: " << real << endl;

    cout << "enter a word: " << left;
    cin >> word >> right;
    cout << "word: " << word << endl;
//file input	
    ifstream inFile( "sample.data" );
    if( !inFile.is_open( ) ) { cout << "file open error" << endl; return 1; }
    string dat;
	cout << setprecision(6);
    inFile >> integer;
    cout << "integer: " << integer << endl;

    inFile >> real;
    cout << "real: " << real << endl;

    inFile >> word;
    cout << "word: " << word << endl;

    inFile.close( );
//file output	
	    ofstream outFile( "sample.out" );
    if( !outFile.is_open( ) ) { cout << "file open error" << endl; return 2; }
    outFile << integer << "," << real << "," << word << endl;
    return 0;
}