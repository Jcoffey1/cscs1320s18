// lab5

#include <stdlib.h>
#include <stdio.h>
//#include <string.h>
#include <math.h>
//https://www.techonthenet.com/c_language/standard_library_functions/string_h/strcmp.php
int count;
int results;
int resultd;
int resultq;
int resultp;

int sum(char * arga, char * argb);
int diff(char * arga, char * argb);
int quot(char * arga, char * argb);
int prod(char * arga, char * argb);

void help( );

int main( int argc, char * argv[ ] )
{
	
	if( argc < 4 )
	{
		printf("Please enter a valid equation");
		return (0);
	}
	//checks if 4 arguments are present
	
	results =  strcmp( *(argv+2), "+" );
	resultd =  strcmp( *(argv+2), "-" );
	resultq =  strcmp( *(argv+2), "/" );
	resultp =  strcmp( *(argv+2), "x" );
	
	if( results != 0 && resultd != 0 && resultq != 0 && resultp != 0)
	{
		printf("Which equation do you want?\nAddition: +\nSubtraction: -\nDivision: /\nMultiplication: x\n");
		return (0);
	}
   for( int count = 0; count < argc; count++ )
   {
		printf( "arg %d: %s\n", count, argv[ count ] );
		//prints the argument count
	  
		if( strcmp( argv[ count ], "help" ) == 0 ) help( );
		//prints out help if called
			
		if( *argv[ count ] >= 'a' && argv[ count ][ 0 ] <= 'z' )
			printf( "First characterer is lowercase letter.\n" );
		//printf happens if the argument is a letter

		printf( "Integer value is: %d\n\n", atoi( argv[ count ] ) );
		//prints the int value for everything entered
   }
		
		//addition
		if( strcmp( *(argv+2), "+" ) == 0 )
		{	
		printf("%s + %s = %d\n", *(argv+1), *(argv+3),sum(*(argv+1),*(argv+3)));
		}
		
		//subtraction
		if( strcmp( *(argv+2), "-" ) == 0 )
		{	
		printf("%s - %s = %d\n", *(argv+1), *(argv+3),diff(*(argv+1),*(argv+3)));
		}
		
		//division
		if( strcmp( *(argv+2), "/" ) == 0 )
		{	
		printf("%s / %s = %d\n", *(argv+1), *(argv+3),quot(*(argv+1),*(argv+3)));
		}
		
		//multiplication
		if( strcmp( *(argv+2), "x" ) == 0 )
		{	
		printf("%s x %s = %d\n", *(argv+1), *(argv+3),prod(*(argv+1),*(argv+3)));
		}
   
   return 0;
} 

void help( )
	{
   printf( "  Help is on its way...\n" );
	}

int sum(char * arga, char * argb)
	{
	return(atoi(arga)+atoi(argb));	
	}

int diff(char * arga, char * argb)
	{
	return(atoi(arga)-atoi(argb));
	}
	
int quot(char * arga, char * argb)
	{
	return(atoi(arga)/atoi(argb));
	}

int prod(char * arga, char * argb)
	{
	return(atoi(arga)*atoi(argb));
	}