#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void help( );

int main( int argc, char * argv[ ] ) {
int answer;
   for( int count = 0; count < argc; count++ ) {
      printf( "arg %d: %s\n", count, argv[ count ] );
      if( strcmp( argv[ count ], "help" ) == 0 ) help( );
      if( *argv[ count ] >= 'a' && argv[ count ][ 0 ] <= 'z' )
         printf( "First characterer is lowercase letter.\n" );
      printf( "Integer value is: %d\n\n", atoi( argv[ count ] ) );
   }
   if (*argv[2] == '+' ) {
              answer = (atoi(argv[1])) + (atoi(argv[3]));
              printf("%s %d", "The answer is :", answer);
              return 0;
          }
           if (*argv[2] == '-' ) {
              answer = (atoi(argv[1])) - (atoi(argv[3]));
              printf("%s %d", "The answer is :", answer);
              return 0;
          }
           if (*argv[2] == 'x' ) {
              answer = (atoi(argv[1]))*(atoi(argv[3]));
              printf("%s %d", "The answer is :", answer);
              return 0;
          }
           if (*argv[2] == '/' ) {
              answer = (atoi(argv[1])) / (atoi(argv[3]));
              printf("%s %d", "The answer is :", answer);
              return 0;
          }
          if (*argv[2] != '+' || *argv[2] != '-' || *argv[2] != 'x' || *argv[2] != '/') {
              printf( "%s", "The third argument must be one of the following operators: +, -, x, / \n" );
              return 0;
          }

   return 0;
} 

void help( ) {
   printf( "  Help is on its way...\n" );
}