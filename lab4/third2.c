//LAB 4

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	// argv array elements
printf( "%10p%10p%3c\n", argv, argv[0], *argv[0] );
printf( "%10c%10p%3c\n", ' ', argv[1], *argv[1] );
printf( "%10c%10p%3c\n", ' ', argv[2], *argv[2] );

printf("\n");

	// argv first array elements
int sz = sizeof (char);
printf( "%10p%10p%3c\n", argv, argv[0], *(argv[0]) );
printf( "%10c%10p%3c\n", ' ', argv[0]+sz, *(argv[0]+sz) );
printf( "%10c%10p%3c\n", ' ', argv[0]+2*sz, *(argv[0]+2*sz) );
printf( "%10c%10p%3c\n", ' ', argv[0]+3*sz, *(argv[0]+3*sz) );

printf("\n");
	// argv array elements by pointer
printf( "%10p%10p%3c\n", argv, *argv, **argv );
printf( "%10c%10p%3c\n", ' ', *(argv+1), **(argv+1) );
printf( "%10c%10p%3c\n", ' ', *(argv+2), **(argv+2) );

printf("\n");
	
	// argv first array element values by pointer
printf( "%10p%10p%3c\n", argv, *argv, **argv );
printf( "%10c%10p%3c\n", ' ', (*argv)+1, *((*argv)+1) );
printf( "%10c%10p%3c\n", ' ', (*argv)+2, *((*argv)+2) );
printf( "%10c%10p%3c\n", ' ', (*argv)+3, *((*argv)+3) );

printf("\n");
	
	// argv second array element values by pointer
printf( "%10p%10p%3c\n", argv, *(argv+1), **(argv+1) );
printf( "%10c%10p%3c\n", ' ', (*(argv+1))+1, *((*(argv+1))+1) );
printf( "%10c%10p%3c\n", ' ', (*(argv+1))+2, *((*(argv+1))+2) );
printf( "%10c%10p%3c\n", ' ', (*(argv+1))+3, *((*(argv+1))+3) );

return (0);
}