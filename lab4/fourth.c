void help( );

int main( int argc, char * argv[ ] ) {
   for( int count = 0; count < argc; count++ ) {
      printf( "arg %d: %s\n", count, argv[ count ] );
      if( strcmp( argv[ count ], "help" ) == 0 ) help( );
      if( *argv[ count ] >= 'a' && argv[ count ][ 0 ] <= 'z' )
         printf( "First characterer is lowercase letter.\n" );
      printf( "Integer value is: %d\n\n", atoi( argv[ count ] ) );
   }

   return 0;
} 

void help( ) {
   printf( "  Help is on its way...\n" );
}